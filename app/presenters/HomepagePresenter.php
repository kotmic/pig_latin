<?php

namespace App\Presenters;


use App\Model\Translator;
use Nette\Application\UI\Form;

class HomepagePresenter extends BasePresenter
{
    /**
     * @inject Translator
     * @var Translator
     */
    public $translator;

	public function renderDefault()
	{

	}

	public function createComponentTranslatorForm() {
        $form = new Form();

        $form->addTextArea('toTranslate', 'Text to translate: ')
            ->setDefaultValue('Text to translate');

        $form->addSubmit('submit', 'Translate to Pig Latin!');

        $form->onSubmit[] = [$this, 'translatorFormSubmitted'];

        return $form;
    }

    public function translatorFormSubmitted(Form $form) {
        $values = $form->getValues(TRUE);

        $this->template->translatedText = $this->translator->translate($values['toTranslate']);

        $this->redrawControl();
    }
}