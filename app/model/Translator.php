<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class Translator
{
	use Nette\SmartObject;

	public function __construct()
	{

	}


	/**
	 * main prekladaci metoda
	 * @return string
	 */
    public function translate($text) {
        $words = explode(' ', $text); //rozdeleni textu na slova
        $result_text = "";             //variable pro vysledny text
        $dia = array(',', '.');        //pole s interpunkci
        $vowel = array('a', 'e', 'i', 'o', 'u'); //pole samohlasek, ktere se vyskytuji jako prvni pismeno ve slovech
        $vowel_y = array('a', 'e', 'i', 'o', 'u', 'y'); //pole samohlasek, ktere jsou uprostred slova
        $vowel_trans = array('a' => 'h', 'e' => 'w', 'i' => 'h', 'o' => 'y', 'u' => 'w'); //pokud slovo zacina samohlaskou potrebuji pole, ktere definuje pocatecni pismeno suffixu

        foreach ($words as $text) {     //prochazeni textu po slovech
            if (in_array(substr($text, -1), $dia)) { //ulozeni diakritickeho znaku, pokud je
                $wdia = substr($text, -1);
                $text = substr($text, 0, strlen($text) - 1); //ulozeni slova bez diakritiky
            } else
                $wdia = "";

            if (ctype_upper(substr($text,0,1))) //osetreni velkych pocatecnich pismen
                $upper = 1;
            else
                $upper = 0;


            if (ctype_alpha($text) and strlen($text) > 3) { //pokud je slovo kratsi nez 3 pismena nebo neobsahuje poze pismena, nema cenu ho prekladat
                $text = strtolower($text); //prevedeni textu na mala pismena
                if (!in_array(substr($text, 0, 1), $vowel)) { //pokud prni pismeno neni samohlaska, tak se provede tato vetev
                    if (substr($text, 0, 2) !== "qu") { //osetreni spravneho prekladu slov zacinajici na qu (e.g. question)
                        for ($i = 1; $i < strlen($text); $i++) { //dokud nenarazime na souhlasku, tak pociame pismena
                            if (in_array(substr($text, $i, 1), $vowel_y))
                                break;
                        }
                        $res = substr($text, $i) . "'" . substr($text, 0, $i) . "ay"; //ulozeni vysledneho slova
                    }else {
                        $res = substr($text, 2) . "'quay"; //ulozeni vysledneho slova v pripade "qu"
                    }
                } else {
                    $res = $text . "'" . $vowel_trans[substr($text, 0, 1)] . "ay"; //pokud je prvni posmeno samohlaska, pocatecni pismeno suffixu se najde v danem poli
                }
            } else {
                $res = $text; //pokud je slovo kratsi nez 4 pismena nebo pokud neobsahuje pouze pismena, neni ho treba prekladat
            }
            if ($upper)$res = strtoupper(substr($res, 0, 1)) . substr($res, 1); //vraceni prvniho pocatecniho pismena

            $result_text.=$res.$wdia." "; //vraceni diakritiky
        }
        return $result_text; //funkce vraci vysledny text
    }

}